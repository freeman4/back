import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Tmdb')
    .setDescription('This API stands between Tmdb API and the freeman\s front-end')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.enableCors({
    origin: /.+/,
    allowedHeaders: '*',
    credentials: true,
  });
  await app.listen(3001);
}
bootstrap();

import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { MovieCheckDto, MovieCheckMessageEnum } from '../common/dto/tmbProxy/tmdb-proxy-movie.dto';
import { MovieResultDto } from '../common/dto/tmdb/tmdb-movie.dto';
import { map, Observable, of, reduce, switchMap } from 'rxjs';
import { TmdbService } from './tmdb.service';
import { ActorResultDto } from '../common/dto/tmdb/tmdb-actor.dto';
import { ActorCheckDto, ActorCheckMessageEnum } from '../common/dto/tmbProxy/tmdb-proxy-actor.dto';

@Controller('tmdb')
export class TmdbController {
  constructor(private readonly tmdbService: TmdbService) {}

  @Get('movie/search/:search')
  searchMovie(@Param('search') search: string): Observable<MovieResultDto[]> {
    return this.tmdbService.searchMovie(search);
  }

  @Get('actor/search/:search')
  searchActor(@Param('search') search: string): Observable<ActorResultDto[]> {
    return this.tmdbService.searchActor(search);
  }

  // TODO: refacto
  @Get('movie')
  checkActorInMovie(
    @Query('movie') search: string,
    @Query('tmdb_actor_id', ParseIntPipe) tmdbActorId: number,
  ): Observable<MovieCheckDto> {
    const filmo$ = this.tmdbService.getFilmo(tmdbActorId);
    const movieId$ = filmo$.pipe(
      reduce((acc: null | number, filmo) => {
        const movie = filmo.cast.find((m) => m.title.toLowerCase() === search.toLowerCase());
        if (movie) return movie.id;
        return acc;
      }, null),
    );
    const movie$ = movieId$.pipe(
      switchMap((movieId) => {
        if (movieId === null) return of(null);
        return this.tmdbService.getMovieById(movieId);
      }),
    );

    return movie$.pipe(
      map((movie) => {
        return {
          message: movie ? MovieCheckMessageEnum.Found : MovieCheckMessageEnum.NotFound,
          movie,
          actor_id: tmdbActorId,
        };
      }),
    );
  }

  // TODO: refacto
  @Get('actor')
  checkMovieInActor(
    @Query('actor') search: string,
    @Query('tmdb_movie_id', ParseIntPipe) tmdbMovieId: number,
  ): Observable<ActorCheckDto> {
    const credits$ = this.tmdbService.getCredits(tmdbMovieId);
    const actorId$ = credits$.pipe(
      reduce((acc: null | number, credit) => {
        credit.cast.forEach((c) => console.log(c.name));
        const actor = credit.cast.find((c) => c.name.toLowerCase() === search.toLocaleLowerCase());
        if (actor) return actor.id;
        return acc;
      }, null),
    );
    const actor$ = actorId$.pipe(
      switchMap((actorId) => {
        if (actorId === null) return of(null);
        return this.tmdbService.getActorById(actorId);
      }),
    );

    return actor$.pipe(
      map((actor) => {
        return {
          message: actor ? ActorCheckMessageEnum.Found : ActorCheckMessageEnum.NotFound,
          actor,
          movie_id: tmdbMovieId,
        };
      }),
    );
  }

  @Get('actor/popular')
  getPopularActor(@Query('pages', ParseIntPipe) pages: number) {
    return this.tmdbService.getPopularActor(pages);
  }
}

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { HttpConfigService } from './axios/http-config.service';
import { TmdbController } from './tmdb.controller';
import { TmdbService } from './tmdb.service';

@Module({
    imports: [
      HttpModule.registerAsync({
        useClass: HttpConfigService,
      }),
    ],
    controllers: [TmdbController],
    providers: [TmdbService],
})
export class TmdbModule {}
import { HttpModuleOptions, HttpModuleOptionsFactory } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";

@Injectable()
export class HttpConfigService implements HttpModuleOptionsFactory {
  createHttpOptions(): HttpModuleOptions | Promise<HttpModuleOptions> {
    return {
      baseURL: 'https://api.themoviedb.org/3/',
      headers: {
        Authorization: `Bearer ${process.env.TMDB_API_TOKEN}`,
      },
    };
  }
}
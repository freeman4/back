import { HttpModule } from '@nestjs/axios';
import { Test } from '@nestjs/testing';
import { MovieCheckMessageEnum } from '../../common/dto/tmbProxy/tmdb-proxy-movie.dto';
import { of } from 'rxjs';
import { HttpConfigService } from '../axios/http-config.service';
import { TmdbController } from '../tmdb.controller';
import { TmdbService } from '../tmdb.service';
import { harryPotterFixture, twelveMonkeysCreditsFixture, twelveMonkeysFixture, twelveMonkeysResultFixture } from './fixture/movie.fixture';
import { brad, bradPitt, bradPittFilmo, popularActors } from './fixture/actor.fixture';
import { ActorCheckMessageEnum } from '../../common/dto/tmbProxy/tmdb-proxy-actor.dto';

describe('TmdbController', () => {
  let tmdbController: TmdbController;
  let tmdbService: TmdbService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [
        HttpModule.registerAsync({
          useClass: HttpConfigService,
        }),
      ],
      controllers: [TmdbController],
      providers: [TmdbService],
    }).compile();

    tmdbService = module.get<TmdbService>(TmdbService);
    tmdbController = module.get<TmdbController>(TmdbController);
  });

  describe('Tmdb API Proxy', () => {
    it('should be defined', () => {
      expect(TmdbController).toBeDefined();
    });
    it('should return a list of movies', (done) => {
      const result = harryPotterFixture.results;
      jest.spyOn(tmdbService, 'searchMovie').mockImplementation(() => of(result));

      tmdbController
        .searchMovie('Harry Potter et les reliques de la mort')
        .subscribe(
          (res) => {
            expect(res).toBe(result);
            done();
          },
        );
    });
    it('should return return a list of actor', (done) => {
      jest.spyOn(tmdbService, 'searchActor')
        .mockImplementation(() => of(brad.results));

        tmdbController
          .searchActor('brad')
          .subscribe(
            (res) => {
              expect(res).toStrictEqual(brad.results);
              done();
            },
          );
    });
    it('should check that a movie is in actor\' filmo and return the movie', (done) => {
      const bradPittId = 287;
      jest.spyOn(tmdbService, 'getFilmo')
        .mockImplementation(() => of(bradPittFilmo));
      jest.spyOn(tmdbService, 'getMovieById')
        .mockImplementation(() => of(twelveMonkeysResultFixture));
      
      tmdbController
        .checkActorInMovie('L\'Armée des 12 singes', bradPittId)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual({
              message: MovieCheckMessageEnum.Found,
              actor_id: 287,
              movie: twelveMonkeysResultFixture,
            });
            done();
          }
        );
    });
    it('should check that an actor is in movie\'s casting and return the actor', (done) => {
      const troieId = 652;
      jest.spyOn(tmdbService, 'getCredits')
        .mockImplementation(() => of(twelveMonkeysCreditsFixture));
      jest.spyOn(tmdbService, 'getActorById')
        .mockImplementation(() => of(bradPitt));
      
      tmdbController
        .checkMovieInActor('Brad Pitt', troieId)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual({
              message: ActorCheckMessageEnum.Found,
              movie_id: troieId,
              actor: bradPitt
            });
            done();
          },
        );
    });
    it('should return a list of popular actors', (done) => {
      jest.spyOn(tmdbService, 'getPopularActor')
        .mockImplementation(() => of(popularActors.results[0]));
      
      tmdbController.getPopularActor(1)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual(popularActors.results[0]);
            done();
          }
        );
    });

  });

});

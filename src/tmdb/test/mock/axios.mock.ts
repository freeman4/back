import { AxiosResponse } from "axios";
import { Observable, pipe } from "rxjs"

export const mockSearchMovieResponse = <T>(data: T, status?) => {

  return new Observable<AxiosResponse<unknown, any>>(subscriber => {
    subscriber.next({
      data,
      status: status ? status : 200,
      statusText: '',
      headers: {},
      config: {},
    });
    setTimeout(() => subscriber.complete(), 1);
  });
}
import { HttpModule, HttpService } from "@nestjs/axios";
import { Test } from "@nestjs/testing";
import { CreditsDto, MovieDto, MovieResultDto } from "../../common/dto/tmdb/tmdb-movie.dto";
import { combineLatest, firstValueFrom } from "rxjs";
import { HttpConfigService } from "../axios/http-config.service";
import { TmdbService } from "../tmdb.service";
import { brad, bradPitt, bradPittFilmo, nataliePortman, nataliePortmanFilmo, popularActors } from "./fixture/actor.fixture";
import { harryPotterFixture, twelveMonkeysCreditsFixture, twelveMonkeysFixture, twelveMonkeysResultFixture } from "./fixture/movie.fixture";
import { mockSearchMovieResponse } from "./mock/axios.mock";
import { ActorDto, ActorResultDto, FilmoDto } from "../../common/dto/tmdb/tmdb-actor.dto";

describe('TmdbService', () => {
  let tmdbService: TmdbService;
  let httpService: HttpService;

  const mockGetWith = <T>(data: T) => {
    jest.spyOn(httpService, 'get')
    .mockImplementation(
      (_) => mockSearchMovieResponse(data),
    );
  }

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [
        HttpModule.registerAsync({
          useClass: HttpConfigService,
        }),
      ],
      providers: [
        TmdbService,
      ],
    }).compile();

    httpService = module.get<HttpService>(HttpService);
    tmdbService = module.get<TmdbService>(TmdbService);
  });

  describe('Tmdb API Proxy service', () => {
    it('should be defined', () => {
      expect(TmdbService).toBeDefined();
    });
    /**
     * completely useless but we're TDDing and want a good test coverage
     */
    it('should return the actor from the corresponding id', (done) => {
      mockGetWith(bradPitt);

      tmdbService.getActorById(287)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual(bradPitt);
            done();
          }
        );
    });
    /**
     * completely useless but we're TDDing and want good test coverage
     */
    it('should return the movie from the corresponding id', (done) => {
      mockGetWith<MovieResultDto>(twelveMonkeysResultFixture);

      tmdbService.getMovieById(63)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual(twelveMonkeysResultFixture);
            done();
          }
        );
    });
    /**
     * kinda unnecessary but made it either way
     * so the test's logic is already implemented for future enhancements
     */
    it('should return a tmdb movie results response', (done) => {
      mockGetWith<MovieDto>(harryPotterFixture);
      
      tmdbService.searchMovie('Harry Potter et les reliques de la mort')
        .subscribe(
          (res: MovieResultDto[]) => {
            expect(res).toStrictEqual(harryPotterFixture.results);
            done();
          },
        );
    });
    it('should return a tmdb actor results response', (done) => {
      mockGetWith<ActorDto>(brad);

      tmdbService.searchActor('brad')
        .subscribe(
          (res: ActorResultDto[]) => {
            expect(res).toStrictEqual(brad.results);
            done();
          },
        );
    });
    it('should return the exact movie(s) that matches search', (done) => {
      mockGetWith<MovieDto>(twelveMonkeysFixture);

      tmdbService.searchMovie('L\'armée des 12 singes', true)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual([twelveMonkeysResultFixture]);
            done();
          },
        );
    });
    it('should return the exact actor(s) that matches search', (done) => {
      mockGetWith<ActorDto>(brad);

      tmdbService.searchActor('Brad Pitt', true)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual([bradPitt]);
            done();
          },
        );
    });
    it('should return movie\'s credits', (done) => {
      mockGetWith<CreditsDto>(twelveMonkeysCreditsFixture);

      tmdbService.getCredits(63)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual(twelveMonkeysCreditsFixture);
            done();
          }
        );
    });
    it('should return actor\'s filmography', (done) => {
      mockGetWith<FilmoDto>(bradPittFilmo);

      tmdbService.getFilmo(bradPitt.id)
        .subscribe(
          (res) => {
            expect(res).toStrictEqual(bradPittFilmo);
            done();
          },
        );
    });
    it('should return true if actor is in cast, false otherwise', () => {
      const bradPittInTwelveMonkeys = tmdbService
        .checkActorInMovie(twelveMonkeysCreditsFixture.cast, bradPitt.id);
      const nataliePortmanInTwelveMonkeys = tmdbService
        .checkActorInMovie(twelveMonkeysCreditsFixture.cast, nataliePortman.id);
      
      expect(bradPittInTwelveMonkeys).toBe(true);
      expect(nataliePortmanInTwelveMonkeys).toBe(false);
    });
    it('should return true if movie is in actor filmo, false otherwise', () => {
      const troyId = 652;
      const troyInBradPitt = tmdbService
        .checkMovieInActor(bradPittFilmo.cast, troyId);
      const troyInNataliePortman = tmdbService
        .checkMovieInActor(nataliePortmanFilmo.cast, troyId);
      
      expect(troyInBradPitt).toBe(true);
      expect(troyInNataliePortman).toBe(false);
    });
    it('should return a list of popular actors', (done) => {
      mockGetWith(popularActors);

      tmdbService.getPopularActor(1)
        .subscribe(
          (res) => {
            expect(popularActors.results).toContain(res);
            done();
          }
        );
    });
  });
});
import { ActorDto, ActorResultDto, FilmoDto } from "../../../common/dto/tmdb/tmdb-actor.dto"

export const bradPitt: ActorResultDto = {
  "adult": false,
  "gender": 2,
  "id": 287,
  "known_for": [
    {
      "adult": false,
      "backdrop_path": "/rr7E0NoGKxvbkb89eR1GwfoYjpA.jpg",
      "genre_ids": [
        18
      ],
      "id": 550,
      "media_type": "movie",
      "original_language": "en",
      "original_title": "Fight Club",
      "overview": "Le narrateur, sans identité précise, vit seul, travaille seul, dort seul, mange seul ses plateaux‐repas pour une personne comme beaucoup d’autres personnes seules qui connaissent la misère humaine, morale et sexuelle. C’est pourquoi il va devenir membre du Fight club, un lieu clandestin où il va pouvoir retrouver sa virilité, l’échange et la communication. Ce club est dirigé par Tyler Durden, une sorte d’anarchiste entre gourou et philosophe qui prêche l’amour de son prochain.",
      "poster_path": "/jSziioSwPVrOy9Yow3XhWIBDjq1.jpg",
      "release_date": "1999-10-15",
      "title": "Fight Club",
      "video": false,
      "vote_average": 8.4,
      "vote_count": 23810
    },
    {
      "adult": false,
      "backdrop_path": "/yVPcPk96E6Qffiyez2oJc7OKD2A.jpg",
      "genre_ids": [
        18,
        28,
        53,
        10752
      ],
      "id": 16869,
      "media_type": "movie",
      "original_language": "en",
      "original_title": "Inglourious Basterds",
      "overview": "Dans la France occupée de 1941, Shosanna Dreyfus assiste à l’exécution de sa famille tombée entre les mains du colonel nazi Hans Landa. Shosanna s’échappe de justesse et s’enfuit à Paris où elle se construit une nouvelle identité en devenant exploitante d’une salle de cinéma. Quelque part ailleurs en Europe, le lieutenant Aldo Raine forme un groupe de soldats juifs américains pour mener des actions punitives particulièrement sanglantes contre les nazis. « Les bâtards », nom sous lequel leurs ennemis vont apprendre à les connaître, se joignent à l’actrice allemande et agent secret Bridget von Hammersmark pour tenter d’éliminer les hauts dignitaires du Troisième Reich. Leurs destins vont se jouer à l’entrée du cinéma où Shosanna est décidée à mettre à exécution une vengeance très personnelle.",
      "poster_path": "/3yOfQLerEO2Qnb1VkSJ8WG3c0Jx.jpg",
      "release_date": "2009-08-19",
      "title": "Inglourious Basterds",
      "video": false,
      "vote_average": 8.2,
      "vote_count": 18477
    },
    {
      "adult": false,
      "backdrop_path": "/bd4RndWxzL2H7aiJKoGgk4clEdD.jpg",
      "genre_ids": [
        80,
        9648,
        53
      ],
      "id": 807,
      "media_type": "movie",
      "original_language": "en",
      "original_title": "Se7en",
      "overview": "Pour conclure sa carrière, l’inspecteur Somerset, vieux flic blasé, tombe à sept jours de la retraite sur un criminel peu ordinaire. John Doe, c’est ainsi que se fait appeler l’assassin, a décidé de nettoyer la société des maux qui la rongent en commettant sept meurtres basés sur les sept péchés capitaux : La gourmandise, l’avarice, la paresse, l’orgueil, la luxure, l’envie et la colère.",
      "poster_path": "/moHx8JGzIdEAMLj1CqDzcLYhMRY.jpg",
      "release_date": "1995-09-22",
      "title": "Seven",
      "video": false,
      "vote_average": 8.3,
      "vote_count": 16845
    }
  ],
  "known_for_department": "Acting",
  "name": "Brad Pitt",
  "popularity": 30.549,
  "profile_path": "/hfkzP7YstXRsj2IM1a8lLz8bvst.jpg"
}

export const nataliePortman: ActorResultDto = {
  "adult": false,
  "gender": 1,
  "id": 524,
  "known_for": [
    {
      "adult": false,
      "backdrop_path": "/7RyHsO4yDXtBv1zUU3mTpHeQ0d5.jpg",
      "genre_ids": [
        12,
        878,
        28
      ],
      "id": 299534,
      "media_type": "movie",
      "original_language": "en",
      "original_title": "Avengers: Endgame",
      "overview": "Après leur défaite face au Titan Thanos qui dans le film précédent s'est approprié toutes les pierres du Gant de l'infini , les Avengers et les Gardiens de la Galaxie ayant survécu à son claquement de doigts qui a pulvérisé « la moitié de toute forme de vie dans l'Univers », Captain America, Thor, Bruce Banner, Natasha Romanoff, War Machine, Tony Stark, Nébula et Rocket, vont essayer de trouver une solution pour ramener leurs coéquipiers disparus et vaincre Thanos en se faisant aider par Ronin alias Clint Barton, Captain Marvel et Ant-Man.",
      "poster_path": "/qXekTarZKRrVfnBwhS6xevt6BGU.jpg",
      "release_date": "2019-04-24",
      "title": "Avengers : Endgame",
      "video": false,
      "vote_average": 8.3,
      "vote_count": 20715
    },
    {
      "adult": false,
      "backdrop_path": "/cDJ61O1STtbWNBwefuqVrRe3d7l.jpg",
      "genre_ids": [
        12,
        14,
        28
      ],
      "id": 10195,
      "media_type": "movie",
      "original_language": "en",
      "original_title": "Thor",
      "overview": "Thor, le héros du nouveau film issu de l'univers Marvel, est un guerrier tout-puissant et arrogant dont les actes téméraires font renaître de nos jours un conflit ancestral. À cause de cela, il est banni du Royaume mythique d’Asgard et est condamné à vivre parmi les humains. Mais lorsque les forces du Mal d’Asgard s’apprêtent à envahir la Terre, Thor découvre enfin ce que signifie \"être un héros\".",
      "poster_path": "/q8pF6s9b9veTQvxTqMDIQf9nJKi.jpg",
      "release_date": "2011-04-21",
      "title": "Thor",
      "video": false,
      "vote_average": 6.8,
      "vote_count": 17975
    },
    {
      "adult": false,
      "backdrop_path": "/uhYoytlNaq46dG81wLmHqaSuzWu.jpg",
      "genre_ids": [
        28,
        12,
        14
      ],
      "id": 76338,
      "media_type": "movie",
      "original_language": "en",
      "original_title": "Thor: The Dark World",
      "overview": "Thor se bat pour restaurer l’ordre à travers l’univers… Mais une ancienne race menée par le menaçant Malekith revient pour précipiter l’univers dans les ténèbres. Face à un ennemi auquel même Odin et Asgard ne peuvent résister, Thor doit s’embarquer dans son plus périlleux voyage jusqu’à ce jour, au cours duquel il retrouvera Jane Foster et sera contraint de tout sacrifier pour sauver l'humanité.",
      "poster_path": "/eAIGX0nlwlb5sMb4uDRGNFqMyG9.jpg",
      "release_date": "2013-10-30",
      "title": "Thor : Le Monde des ténèbres",
      "video": false,
      "vote_average": 6.6,
      "vote_count": 14662
    }
  ],
  "known_for_department": "Acting",
  "name": "Natalie Portman",
  "popularity": 25.377,
  "profile_path": "/nYNPUjkseE8xvZoTTGtt4nlx4dG.jpg"
}

export const brad: ActorDto = {
  "page": 1,
  "results": [
    {
      "adult": false,
      "gender": 2,
      "id": 287,
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/rr7E0NoGKxvbkb89eR1GwfoYjpA.jpg",
          "genre_ids": [
            18
          ],
          "id": 550,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Fight Club",
          "overview": "Le narrateur, sans identité précise, vit seul, travaille seul, dort seul, mange seul ses plateaux‐repas pour une personne comme beaucoup d’autres personnes seules qui connaissent la misère humaine, morale et sexuelle. C’est pourquoi il va devenir membre du Fight club, un lieu clandestin où il va pouvoir retrouver sa virilité, l’échange et la communication. Ce club est dirigé par Tyler Durden, une sorte d’anarchiste entre gourou et philosophe qui prêche l’amour de son prochain.",
          "poster_path": "/jSziioSwPVrOy9Yow3XhWIBDjq1.jpg",
          "release_date": "1999-10-15",
          "title": "Fight Club",
          "video": false,
          "vote_average": 8.4,
          "vote_count": 23810
        },
        {
          "adult": false,
          "backdrop_path": "/yVPcPk96E6Qffiyez2oJc7OKD2A.jpg",
          "genre_ids": [
            18,
            28,
            53,
            10752
          ],
          "id": 16869,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Inglourious Basterds",
          "overview": "Dans la France occupée de 1941, Shosanna Dreyfus assiste à l’exécution de sa famille tombée entre les mains du colonel nazi Hans Landa. Shosanna s’échappe de justesse et s’enfuit à Paris où elle se construit une nouvelle identité en devenant exploitante d’une salle de cinéma. Quelque part ailleurs en Europe, le lieutenant Aldo Raine forme un groupe de soldats juifs américains pour mener des actions punitives particulièrement sanglantes contre les nazis. « Les bâtards », nom sous lequel leurs ennemis vont apprendre à les connaître, se joignent à l’actrice allemande et agent secret Bridget von Hammersmark pour tenter d’éliminer les hauts dignitaires du Troisième Reich. Leurs destins vont se jouer à l’entrée du cinéma où Shosanna est décidée à mettre à exécution une vengeance très personnelle.",
          "poster_path": "/3yOfQLerEO2Qnb1VkSJ8WG3c0Jx.jpg",
          "release_date": "2009-08-19",
          "title": "Inglourious Basterds",
          "video": false,
          "vote_average": 8.2,
          "vote_count": 18477
        },
        {
          "adult": false,
          "backdrop_path": "/bd4RndWxzL2H7aiJKoGgk4clEdD.jpg",
          "genre_ids": [
            80,
            9648,
            53
          ],
          "id": 807,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Se7en",
          "overview": "Pour conclure sa carrière, l’inspecteur Somerset, vieux flic blasé, tombe à sept jours de la retraite sur un criminel peu ordinaire. John Doe, c’est ainsi que se fait appeler l’assassin, a décidé de nettoyer la société des maux qui la rongent en commettant sept meurtres basés sur les sept péchés capitaux : La gourmandise, l’avarice, la paresse, l’orgueil, la luxure, l’envie et la colère.",
          "poster_path": "/moHx8JGzIdEAMLj1CqDzcLYhMRY.jpg",
          "release_date": "1995-09-22",
          "title": "Seven",
          "video": false,
          "vote_average": 8.3,
          "vote_count": 16845
        }
      ],
      "known_for_department": "Acting",
      "name": "Brad Pitt",
      "popularity": 30.549,
      "profile_path": "/hfkzP7YstXRsj2IM1a8lLz8bvst.jpg"
    },
    {
      "adult": false,
      "gender": 2,
      "id": 18,
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/sWWwq8MsfI3Zrn4Zz0HENnhp70J.jpg",
          "genre_ids": [
            16,
            10751
          ],
          "id": 12,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Finding Nemo",
          "overview": "Dans les eaux tropicales de la Grande Barrière de corail, un poisson‐clown du nom de Marin mène une existence paisible avec son fils unique, Nemo. Redoutant l’océan et ses risques imprévisibles, il fait de son mieux pour protéger son fils. Comme tous les petits poissons de son âge, celui‐ci rêve pourtant d’explorer les mystérieux récifs. Lorsque Nemo disparaît, Marin devient malgré lui le héros d’une quête unique et palpitante. Le pauvre papa ignore que son rejeton à écailles a été emmené jusque dans l’aquarium d’un dentiste. Marin ne s’engagera pas seul dans l’aventure : la jolie Dory, un poisson‐chirurgien bleu à la mémoire défaillante et au grand cœur, va se révéler d’une aide précieuse. Les deux poissons vont affronter d’innombrables dangers, mais l’optimisme de Dory va pousser Marin à surmonter toutes ses peurs.",
          "poster_path": "/8zR2vXoXfdlknEYjfHvCbb1rJbI.jpg",
          "release_date": "2003-05-30",
          "title": "Le Monde de Nemo",
          "video": false,
          "vote_average": 7.8,
          "vote_count": 16222
        },
        {
          "adult": false,
          "backdrop_path": "/xgDj56UWyeWQcxQ44f5A3RTWuSs.jpg",
          "genre_ids": [
            16,
            35,
            10751,
            14
          ],
          "id": 2062,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Ratatouille",
          "overview": "Rémy est un jeune rat qui rêve de devenir un grand chef français. Ni l’opposition de sa famille, ni le fait d’être un rongeur dans une profession qui les déteste ne le démotivent. Rémy est prêt à tout pour vivre sa passion de la cuisine… et le fait d’habiter dans les égouts du restaurant ultra coté de la star des fourneaux, Auguste Gusteau, va lui en donner l’occasion\u202f! Malgré le danger et les pièges, la tentation est grande de s’aventurer dans cet univers interdit. Écartelé entre son rêve et sa condition, Rémy va découvrir le vrai sens de l’aventure, de l’amitié, de la famille… et comprendre qu’il doit trouver le courage d’être ce qu’il est : un rat qui veut être un grand chef…",
          "poster_path": "/iFcWBdTPeHQDS3OQxBcH3QaYXYv.jpg",
          "release_date": "2007-06-28",
          "title": "Ratatouille",
          "video": false,
          "vote_average": 7.8,
          "vote_count": 13788
        },
        {
          "adult": false,
          "backdrop_path": "/9zAhWbqVxYH3Hw7Z4LY8FG7l8E1.jpg",
          "genre_ids": [
            12,
            16,
            35,
            10751
          ],
          "id": 127380,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Finding Dory",
          "overview": "Dory, le poisson chirurgien bleu amnésique, retrouve ses amis Nemo et Marlin. Tous trois se lancent à la recherche du passé de Dory. Pourra‐t‐elle retrouver ses souvenirs\u202f? Qui sont ses parents\u202f? Et où a‐t‐elle bien pu apprendre à parler la langue des baleines\u202f?",
          "poster_path": "/cBXWl2y6opjVPNwue5lAQR9R4fq.jpg",
          "release_date": "2016-06-16",
          "title": "Le Monde de Dory",
          "video": false,
          "vote_average": 7,
          "vote_count": 10501
        }
      ],
      "known_for_department": "Acting",
      "name": "Brad Garrett",
      "popularity": 14.935,
      "profile_path": "/813ffbpoaoaJRdoe1yrbivboWKp.jpg"
    }
  ],
  "total_pages": 1,
  "total_results": 2
}

export const bradPittFilmo: FilmoDto = {
  "id": 287,
  "cast": [
    {
      "adult": false,
      "backdrop_path": "/o67MmFburfckl6iPa4DVkranLi3.jpg",
      "genre_ids": [
        14,
        18,
        10749
      ],
      "id": 297,
      "original_language": "en",
      "original_title": "Meet Joe Black",
      "overview": "Une nuit, le magnat William Parrish ressent une violente douleur tandis qu'une voix surgissant des ténèbres lui annonce sa mort prochaine. À ce moment-la, un jeune inconnu se présente à son domicile pour l'accompagner vers son dernier voyage. Ce messager de l'au-delà impose a Parrish de l'héberger chez lui afin de lui donner l'occasion de partager un temps les expériences, les joies, les émotions et les drames des vivants, qui semblent lui être étrangers. En l'espace de trois jours, Joe Black révèlera toute la famille Parrish à elle-même.",
      "poster_path": "/6QtYcXs5qysRFpw8cN1dFUmfhxh.jpg",
      "release_date": "1998-11-12",
      "title": "Rencontre avec Joe Black",
      "video": false,
      "vote_average": 7.2,
      "vote_count": 3844,
      "popularity": 43.547,
      "character": "Joe Black / Young Man in Coffee Shop",
      "credit_id": "52fe4234c3a36847f800bdbb",
      "order": 0
    },
    {
      "adult": false,
      "backdrop_path": "/vXMn5Acau1pW9hzun5P4yM7hQ6r.jpg",
      "genre_ids": [
        12,
        36,
        10752
      ],
      "id": 652,
      "original_language": "en",
      "original_title": "Troy",
      "overview": "Dans la Grèce antique, l'enlèvement d'Hélène, reine de Sparte, par Pâris, prince de Troie, est une insulte que le roi Ménélas ne peut supporter. L'honneur familial étant en jeu, Agamemnon, frère de Ménélas et puissant roi de Mycènes, réunit toutes les armées grecques afin de faire sortir Hélène de Troie. Mais en réalité, la sauvegarde de l'honneur familial n'est qu'un prétexte pris par Agamemnon pour cacher sa terrible avidité. Celui-ci cherche en fait à contrôler Troie et à agrandir son vaste empire. Aucune armée n'a jamais réussi à pénétrer dans la cité fortifiée, sur laquelle veillent le roi Priam et le prince Hector. L'issue de la guerre de Troie dépendra notamment d'un homme, Achille, connu comme le plus grand guerrier de son époque. Arrogant, rebelle, et réputé invincible, celui-ci n'a d'attache pour rien ni personne si ce n'est sa propre gloire…",
      "poster_path": "/uHleivj7qVa8RtSUggvTpkt1cov.jpg",
      "release_date": "2004-05-03",
      "title": "Troie",
      "video": false,
      "vote_average": 7.1,
      "vote_count": 8404,
      "popularity": 55.41,
      "character": "Achilles",
      "credit_id": "52fe4264c3a36847f801b083",
      "order": 0
    },
    {
      "adult": false,
      "backdrop_path": "/3fChciF2G1wXHsyTfJD9y7uN6Il.jpg",
      "genre_ids": [
        27,
        18,
        14
      ],
      "id": 628,
      "original_language": "en",
      "original_title": "Interview with the Vampire",
      "overview": "San Francisco dans les années 90. Un jeune journaliste, Malloy, s'entretient dans une chambre avec un homme élégant, à l'allure aristocratique et au visage blafard, Louis, qui lui fait de bien étranges confidences. Malloy, subjugué par la séduction de son interlocuteur lui demande, à l'aube, de le faire pénétrer dans son monde, celui des vampires.",
      "poster_path": "/efQVbPAfIchr87GCHsmdZaJ3cGM.jpg",
      "release_date": "1994-11-11",
      "title": "Entretien avec un vampire",
      "video": false,
      "vote_average": 7.4,
      "vote_count": 4596,
      "popularity": 30.966,
      "character": "Louis de Pointe du Lac",
      "credit_id": "52fe4260c3a36847f80199f9",
      "order": 0
    },
    {
      "original_language": "en",
      "original_title": "Twelve Monkeys",
      "id": 63,
      "video": false,
      "title": "L'Armée des 12 singes",
      "overview": "Nous sommes en l’an 2035. Les quelques milliers d’habitants qui restent sur notre planète sont contraints de vivre sous terre. La surface du globe est devenue inhabitable à la suite d’un virus ayant décimé 99\u202f% de la population. Les survivants mettent tous leurs espoirs dans un voyage à travers le temps pour découvrir les causes de la catastrophe et la prévenir. C’est James Cole, hanté depuis des années par une image incompréhensible, qui est désigné pour cette mission.",
      "release_date": "1995-12-29",
      "vote_count": 6792,
      "adult": false,
      "backdrop_path": "/rPRcZAOMJLcUX98Jqo9iqlKY998.jpg",
      "vote_average": 7.6,
      "genre_ids": [
        878,
        53,
        9648
      ],
      "poster_path": "/81XKdqLWzYmI3F4IdFCep3chNGs.jpg",
      "popularity": 26.626,
      "character": "Jeffrey Goines",
      "credit_id": "52fe4212c3a36847f8001b39",
      "order": 2
    },

  ],
  "crew": [
    {
      "id": 402970,
      "video": false,
      "vote_count": 1,
      "vote_average": 5.0,
      "title": "Notes on an American Film Director at Work",
      "release_date": "2008-01-01",
      "original_language": "en",
      "original_title": "Notes on an American Film Director at Work",
      "genre_ids": [
        99
      ],
      "backdrop_path": "/1Q0FrRbywFPjseam7mwt6MYKXe7.jpg",
      "adult": false,
      "overview": "",
      "poster_path": "/8Ah2goGOhoR1i7jwOOaie10reoS.jpg",
      "popularity": 2.689,
      "credit_id": "5ebdf4f1bc8abc0022c2e548",
      "department": "Crew",
      "job": "Thanks"
    },
    {
      "vote_average": 5.0,
      "overview": "Robert d'Eglantine invite ses amis à passer quelques jours dans une vieille maison de campagne où il leur propose de se lancer dans une aventure audacieuse à la destination surprenante.",
      "release_date": "2016-01-13",
      "title": "House of Time",
      "adult": false,
      "backdrop_path": "/2XuHnnAhTaLAw1RlxXDPIjCgtYC.jpg",
      "genre_ids": [
        35,
        878,
        53
      ],
      "vote_count": 6,
      "original_language": "en",
      "original_title": "House of Time",
      "poster_path": "/vNbRZDp3E4wNFwe0BoGhBtJrgKp.jpg",
      "id": 369773,
      "video": false,
      "popularity": 1.686,
      "credit_id": "60bd62d6b0460500571f3fae",
      "department": "Crew",
      "job": "Thanks"
    }
  ]
}

export const nataliePortmanFilmo: FilmoDto = {
  "id": 524,
  "cast": [
    {
      "original_title": "V for Vendetta",
      "poster_path": "/4HFt1jyjZD3MssiyNPJLToqGd1v.jpg",
      "video": false,
      "vote_average": 7.9,
      "overview": "Londres, au 21ème siècle… Evey Hammond ne veut rien oublier de l’homme qui lui sauva la vie et lui permit de dominer ses peurs les plus lointaines. Mais il fut un temps où elle n’aspirait qu’à l’anonymat pour échapper à une police secrète omnipotente. Comme tous ses concitoyens, trop vite soumis, elle acceptait que son pays ait perdu son âme et se soit donné en masse au tyran Sutler et à ses partisans. Une nuit, alors que deux « gardiens de l’ordre » s’apprêtaient à la violer dans une rue déserte, Evey vit surgir son libérateur. Et rien ne fut plus comme avant. Son apprentissage commença quelques semaines plus tard sous la tutelle de « V ». Evey ne connaîtrait jamais son nom et son passé, ne verrait jamais son visage atrocement brûlé et défiguré, mais elle deviendrait à la fois son unique disciple, sa seule amie et le seul amour d’une vie sans amour…",
      "release_date": "2006-02-23",
      "vote_count": 12092,
      "adult": false,
      "backdrop_path": "/sFEYsEfzTx7hhjetlNrme8B5OUo.jpg",
      "title": "V pour Vendetta",
      "genre_ids": [
        28,
        53,
        14
      ],
      "id": 752,
      "original_language": "en",
      "popularity": 39.748,
      "character": "Evey Hammond",
      "credit_id": "52fe4270c3a36847f801ec1d",
      "order": 0
    },
    {
      "adult": false,
      "backdrop_path": "/ahGSlXunI3Aa0MV4IXlpjYVSVWI.jpg",
      "genre_ids": [
        35,
        18,
        10749
      ],
      "id": 10564,
      "original_language": "en",
      "original_title": "Where the Heart Is",
      "overview": "Novalee Nation, une adolescente de 17 ans, et son amoureux partent du Tennessee direction la Californie afin d'élever dans les meilleures conditions possibles leur futur bébé. En cours de route, Novalee doit s'acheter une nouvelle paire de chaussures dans un Wal-Mart et le petit ami profite de cette escale à Oklahoma pour l'abandonner lâchement.Novalee prend alors son courage à deux mains et décide d'établir sa résidence dans le Wal-Mart. De nombreuses personnes viennent la soutenir, dont Lexie Coop, une infirmière qui se préoccupe de son état de santé. Tout cet entourage va s'efforcer de prodiguer les plus grands soins à la jeune maman et à sa progéniture.",
      "poster_path": "/9MpxZGX0Wetb87p8uj1joSaVMQd.jpg",
      "release_date": "2000-04-27",
      "title": "Où le cœur nous mène",
      "video": false,
      "vote_average": 7.1,
      "vote_count": 548,
      "popularity": 17.781,
      "character": "Novalee Nation",
      "credit_id": "52fe43889251416c75014223",
      "order": 0
    }
  ],
  "crew": [
    {
      "adult": false,
      "backdrop_path": null,
      "genre_ids": [
        35
      ],
      "id": 12418,
      "original_language": "en",
      "original_title": "Eve",
      "overview": "",
      "poster_path": "/pRyHW5otUPAIHFiCSyQTBNiI562.jpg",
      "release_date": "2008-08-27",
      "title": "Eve",
      "video": false,
      "vote_average": 6.0,
      "vote_count": 1,
      "popularity": 1.4,
      "credit_id": "52fe44de9251416c75043cb3",
      "department": "Directing",
      "job": "Director"
    },
    {
      "adult": false,
      "backdrop_path": "/n1K3L6Xh6L6F1vxE7PJwDLiz8AF.jpg",
      "genre_ids": [
        18
      ],
      "id": 336029,
      "original_language": "he",
      "original_title": "A Tale of Love and Darkness",
      "overview": "Amos Oz se souvient de son enfance, en 1945, dans une Jérusalem sous mandat britannique. Il se remémore sa relation avec son père, Arieh, et sa mère, Fania. Celle-ci lui raconte régulièrement ses souvenirs de jeunesse, en Pologne. Le garçon, qui aime beaucoup lire, passe régulièrement du temps avec des amis de ses parents, qui n'ont pas eu d'enfant. Avec eux, il rencontre des palestiniens, dont une jeune fille de son âge.",
      "poster_path": "/8at5heClGEMUqVsOedz4g2ZgYZF.jpg",
      "release_date": "2015-09-03",
      "title": "Une histoire d'amour et de ténèbres",
      "video": false,
      "vote_average": 5.5,
      "vote_count": 111,
      "popularity": 8.849,
      "credit_id": "552f4214c3a36856cd000fda",
      "department": "Directing",
      "job": "Director"
    }
  ]
}


export const popularActors = {
  "total_page": 1,
  "total_results": 1,
  "page": 1,
  "results": [
    {
      "adult": false,
      "gender": 2,
      "id": 1136406,
      "known_for": [
        {
          "adult": false,
          "backdrop_path": "/lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
          "genre_ids": [
            12,
            28,
            878
          ],
          "id": 299536,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Avengers: Infinity War",
          "overview": "Les Avengers et leurs alliés devront être prêts à tout sacrifier pour neutraliser le redoutable Thanos avant que son attaque éclair ne conduise à la destruction complète de l’univers.",
          "poster_path": "/hSfuKPtyEryeFzapZ8UgZd4aESu.jpg",
          "release_date": "2018-04-25",
          "title": "Avengers : Infinity War",
          "video": false,
          "vote_average": 8.3,
          "vote_count": 24335
        },
        {
          "adult": false,
          "backdrop_path": "/7RyHsO4yDXtBv1zUU3mTpHeQ0d5.jpg",
          "genre_ids": [
            12,
            878,
            28
          ],
          "id": 299534,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Avengers: Endgame",
          "overview": "Après leur défaite face au Titan Thanos qui dans le film précédent s'est approprié toutes les pierres du Gant de l'infini , les Avengers et les Gardiens de la Galaxie ayant survécu à son claquement de doigts qui a pulvérisé « la moitié de toute forme de vie dans l'Univers », Captain America, Thor, Bruce Banner, Natasha Romanoff, War Machine, Tony Stark, Nébula et Rocket, vont essayer de trouver une solution pour ramener leurs coéquipiers disparus et vaincre Thanos en se faisant aider par Ronin alias Clint Barton, Captain Marvel et Ant-Man.",
          "poster_path": "/qXekTarZKRrVfnBwhS6xevt6BGU.jpg",
          "release_date": "2019-04-24",
          "title": "Avengers : Endgame",
          "video": false,
          "vote_average": 8.3,
          "vote_count": 20775
        },
        {
          "adult": false,
          "backdrop_path": "/7FWlcZq3r6525LWOcvO9kNWurN1.jpg",
          "genre_ids": [
            12,
            28,
            878
          ],
          "id": 271110,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "Captain America: Civil War",
          "overview": "Steve Rogers est désormais à la tête des Avengers, dont la mission est de protéger l’humanité. À la suite d’une de leurs interventions qui a causé d’importants dégâts collatéraux, le gouvernement décide de mettre en place un organisme de commandement et de supervision. Cette nouvelle donne provoque une scission au sein de l’équipe : Steve Rogers reste attaché à sa liberté de s’engager sans ingérence gouvernementale, tandis que d’autres se rangent derrière Tony Stark, qui contre toute attente, décide de se soumettre au gouvernement…",
          "poster_path": "/py8Js6LEl10XhUjkoOj6UGRrSFp.jpg",
          "release_date": "2016-04-27",
          "title": "Captain America : Civil War",
          "video": false,
          "vote_average": 7.4,
          "vote_count": 19630
        }
      ],
      "known_for_department": "Acting",
      "name": "Tom Holland",
      "popularity": 122.665,
      "profile_path": "/2qhIDp44cAqP2clOgt2afQI07X8.jpg"
    },
    {
      "adult": false,
      "gender": 2,
      "id": 21523,
      "known_for": [
        {
          "backdrop_path": "/dpzC0ACzwkVporKvzZGvNFOBFr6.jpg",
          "first_air_date": "2005-03-27",
          "genre_ids": [
            18
          ],
          "id": 1416,
          "media_type": "tv",
          "name": "Grey's Anatomy",
          "origin_country": [
            "US"
          ],
          "original_language": "en",
          "original_name": "Grey's Anatomy",
          "overview": "Meredith Grey, fille d'un chirurgien très réputé, commence son internat de première année en médecine chirurgicale dans un hôpital de Seattle. La jeune femme s'efforce de maintenir de bonnes relations avec ses camarades internes, mais dans ce métier difficile la compétition fait rage.",
          "poster_path": "/clnyhPqj1SNgpAdeSS6a6fwE6Bo.jpg",
          "vote_average": 8.3,
          "vote_count": 7882
        },
        {
          "adult": false,
          "backdrop_path": "/bYimqNnizPUCnL5HOdoCW02IGmH.jpg",
          "genre_ids": [
            18,
            35,
            10749
          ],
          "id": 74643,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "The Artist",
          "overview": "Hollywood 1927. George Valentin est une vedette du cinéma muet à qui tout sourit. L’arrivée des films parlants va le faire sombrer dans l’oubli. Peppy Miller, jeune figurante, va elle, être propulsée au firmament des stars.",
          "poster_path": "/uV4qg1nVKftvyqBejgU44R4QkCk.jpg",
          "release_date": "2011-10-12",
          "title": "The Artist",
          "video": false,
          "vote_average": 7.4,
          "vote_count": 2670
        },
        {
          "adult": false,
          "backdrop_path": "/kP2DsgcIwuubm3WBvmxJcWFKzL2.jpg",
          "genre_ids": [
            53,
            18,
            9648
          ],
          "id": 3594,
          "media_type": "movie",
          "original_language": "en",
          "original_title": "The Number 23",
          "overview": "Walter, un contrôleur animalier, mène une vie tranquille avec sa femme pâtissière, Agatha, et leur fils, Robin. Pour son anniversaire, Agatha offre à Walter un livre qui détaille l'obsession meurtrière de l'étrange détective Petitou pour le nombre 23. Peu à peu, l'univers du livre envahit son quotidien...",
          "poster_path": "/4vdMg2MLlch27pG1CKuB3vLeOac.jpg",
          "release_date": "2007-02-23",
          "title": "Le Nombre 23",
          "video": false,
          "vote_average": 6.4,
          "vote_count": 2662
        }
      ],
      "known_for_department": "Acting",
      "name": "Ed Lauter",
      "popularity": 146.909,
      "profile_path": "/5q1CNG6M4cKIrFjFmy7typjEPqt.jpg"
    }
  ]
}

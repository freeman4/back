import { CreditsDto, MovieDto, MovieResultDto } from "../../../common/dto/tmdb/tmdb-movie.dto"

export const harryPotterFixture: MovieDto = {
  "page": 1,
  "results": [
    {
      "adult": false,
      "backdrop_path": "/n5A7brJCjejceZmHyujwUTVgQNC.jpg",
      "genre_ids": [
        14,
        12
      ],
      "id": 12445,
      "original_language": "en",
      "original_title": "Harry Potter and the Deathly Hallows: Part 2",
      "overview": "Le combat entre les puissances du bien et du mal de l’univers des sorciers se transforme en guerre sans merci. L’école Poudlard est désormais sous la coupe du professeur Rogue et des force des ténèbres. Les enjeux sont devenus considérables et personne n’est plus en sécurité alors que se rapproche l’ultime épreuve de force avec Voldemort…",
      "popularity": 190.474,
      "poster_path": "/alQOPmKEPHkH4BLMEla1vTNYrUr.jpg",
      "release_date": "2011-07-07",
      "title": "Harry Potter et les Reliques de la mort : 2ème partie",
      "video": false,
      "vote_average": 8.1,
      "vote_count": 17057
    },
    {
      "adult": false,
      "backdrop_path": "/fXRXpzxUApE3OuXhIqsdavQjCVa.jpg",
      "genre_ids": [
        12,
        14
      ],
      "id": 12444,
      "original_language": "en",
      "original_title": "Harry Potter and the Deathly Hallows: Part 1",
      "overview": "Le pouvoir de Voldemort s’étend. Celui‐ci contrôle maintenant le Ministère de la Magie et Poudlard. Harry, Ron et Hermione décident de terminer le travail commencé par Dumbledore, et de retrouver les derniers Horcruxes pour vaincre le Seigneur des Ténèbres. Mais il reste bien peu d’espoir aux trois sorciers, qui doivent réussir à tout prix.",
      "popularity": 180.659,
      "poster_path": "/yJm61MmTMjOmNXxPxdoaIkdqnOm.jpg",
      "release_date": "2010-10-17",
      "title": "Harry Potter et les Reliques de la mort : 1ère partie",
      "video": false,
      "vote_average": 7.8,
      "vote_count": 15839
    }
  ],
  "total_pages": 1,
  "total_results": 2
}

export const twelveMonkeysResultFixture: MovieResultDto = {
  "adult": false,
  "backdrop_path": "/rPRcZAOMJLcUX98Jqo9iqlKY998.jpg",
  "genre_ids": [
    878,
    53,
    9648
  ],
  "id": 63,
  "original_language": "en",
  "original_title": "Twelve Monkeys",
  "overview": "Nous sommes en l’an 2035. Les quelques milliers d’habitants qui restent sur notre planète sont contraints de vivre sous terre. La surface du globe est devenue inhabitable à la suite d’un virus ayant décimé 99\u202f% de la population. Les survivants mettent tous leurs espoirs dans un voyage à travers le temps pour découvrir les causes de la catastrophe et la prévenir. C’est James Cole, hanté depuis des années par une image incompréhensible, qui est désigné pour cette mission.",
  "popularity": 40.692,
  "poster_path": "/81XKdqLWzYmI3F4IdFCep3chNGs.jpg",
  "release_date": "1995-12-29",
  "title": "L'Armée des 12 singes",
  "video": false,
  "vote_average": 7.6,
  "vote_count": 6770
}

export const twelveMonkeysCreditsFixture: CreditsDto = {
  "id": 63,
  "cast": [
    {
      "adult": false,
      "gender": 2,
      "id": 62,
      "known_for_department": "Acting",
      "name": "Bruce Willis",
      "original_name": "Bruce Willis",
      "popularity": 37.655,
      "profile_path": "/caX3KtMU42EP3VLRFFBwqIIrch5.jpg",
      "cast_id": 41,
      "character": "James Cole",
      "credit_id": "52fe4212c3a36847f8001b31",
      "order": 0
    },
    {
      "adult": false,
      "gender": 1,
      "id": 289,
      "known_for_department": "Acting",
      "name": "Madeleine Stowe",
      "original_name": "Madeleine Stowe",
      "popularity": 12.281,
      "profile_path": "/9QxwBnERu7KMMe0kQhAGMPd2WZS.jpg",
      "cast_id": 42,
      "character": "Dr. Kathryn Railly",
      "credit_id": "52fe4212c3a36847f8001b35",
      "order": 1
    },
    {
      "adult": false,
      "gender": 2,
      "id": 287,
      "known_for_department": "Acting",
      "name": "Brad Pitt",
      "original_name": "Brad Pitt",
      "popularity": 22.765,
      "profile_path": "/hfkzP7YstXRsj2IM1a8lLz8bvst.jpg",
      "cast_id": 43,
      "character": "Jeffrey Goines",
      "credit_id": "52fe4212c3a36847f8001b39",
      "order": 2
    },
    {
      "adult": false,
      "gender": 2,
      "id": 290,
      "known_for_department": "Acting",
      "name": "Christopher Plummer",
      "original_name": "Christopher Plummer",
      "popularity": 5.246,
      "profile_path": "/iZh3s9Vy9vYD4DYnAda6C1kdeco.jpg",
      "cast_id": 44,
      "character": "Dr. Goines",
      "credit_id": "52fe4212c3a36847f8001b3d",
      "order": 3
    }
  ],
  "crew": [
    {
      "adult": false,
      "gender": 1,
      "id": 8222,
      "known_for_department": "Costume & Make-Up",
      "name": "Julie Weiss",
      "original_name": "Julie Weiss",
      "popularity": 1.4,
      "profile_path": "/vKsnRoJXkEOf1B4wzsWOlRBadLW.jpg",
      "credit_id": "52fe4212c3a36847f8001b2d",
      "department": "Costume & Make-Up",
      "job": "Costume Design"
    },
    {
      "adult": false,
      "gender": 2,
      "id": 280,
      "known_for_department": "Directing",
      "name": "Terry Gilliam",
      "original_name": "Terry Gilliam",
      "popularity": 2.869,
      "profile_path": "/kODccJVnrt4ioPbJVEnkkKPHbG7.jpg",
      "credit_id": "52fe4212c3a36847f8001ac7",
      "department": "Directing",
      "job": "Director"
    },
    {
      "adult": false,
      "gender": 1,
      "id": 281,
      "known_for_department": "Writing",
      "name": "Janet Peoples",
      "original_name": "Janet Peoples",
      "popularity": 1.166,
      "profile_path": null,
      "credit_id": "52fe4212c3a36847f8001ad3",
      "department": "Writing",
      "job": "Screenplay"
    }
  ]
}

export const twelveMonkeysFixture: MovieDto = {
  "page": 1,
  "results": [
    twelveMonkeysResultFixture
  ],
  "total_pages": 1,
  "total_results": 1,
}

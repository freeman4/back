import { HttpService } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";
import { AxiosResponse } from "axios";
import { ActorDto, ActorResultDto, FilmoCastDto, FilmoDto } from "../common/dto/tmdb/tmdb-actor.dto";
import { CastDto, CreditsDto, MovieDto, MovieResultDto } from "../common/dto/tmdb/tmdb-movie.dto";
import { filter, map, Observable, reduce } from "rxjs";


@Injectable()
export class TmdbService {
  
  constructor(private httpService: HttpService) {}

  private filterMovies(movieResults: MovieResultDto[], search: string) {

    return movieResults.filter((movie) => {
      // TODO: use js-levenshtein for a more intelligent match
      return movie.title.toLowerCase() === search.toLowerCase();
    });
  }

  private filterActors(actorResults: ActorResultDto[], search: string) {
    return actorResults.filter((actor) => {
      // TODO: use js-levenshtein for a more intelligent match
      return actor.name.toLowerCase() === search.toLowerCase();
    });
  }

  getActorById(id: number): Observable<ActorResultDto> {
    return this.httpService.get(`person/${id}`)
      .pipe(
        map((axiosRes: AxiosResponse<ActorResultDto>) => axiosRes.data),
      );
  }

  getMovieById(id: number): Observable<MovieResultDto> {
    return this.httpService.get(`movie/${id}`)
      .pipe(
        map((axiosRes: AxiosResponse<MovieResultDto>) => axiosRes.data),
      );
  }

  searchMovie(search: string, strict = false): Observable<MovieResultDto[]> {
    return this.httpService.get('search/movie', {
      params: {
        language: 'fr-FR',
        query: search,
        include_adult: true,
      },
    }).pipe(
      map((axiosRes: AxiosResponse<MovieDto>) => {
        return strict ?
          this.filterMovies(axiosRes.data.results, search) :
          axiosRes.data.results;
      }),
    );
  }

  searchActor(search: string, strict = false): Observable<ActorResultDto[]> {
    return this.httpService.get('search/person', {
      params: {
        language: 'fr-FR',
        query: search,
        include_adult: true,
      },
    }).pipe(
      map((axiosRes: AxiosResponse<ActorDto>) => {
        return strict ?
          this.filterActors(axiosRes.data.results, search) :
          axiosRes.data.results;
      }),
    );
  }

  getCredits(movieId: number): Observable<CreditsDto> {
    return this.httpService.get(`movie/${movieId}/credits`, {
      params: {
        language: 'fr-FR',
      },
    }).pipe(
      map((axiosRes: AxiosResponse<CreditsDto>) => {
        return axiosRes.data;
      }),
    );
  }

  getFilmo(actorId: number): Observable<FilmoDto> {
    return this.httpService.get(`person/${actorId}/movie_credits`, {
      params: {
        language: 'fr-FR',
      },
    }).pipe(
      map((axiosRes: AxiosResponse<FilmoDto>) => {
        return axiosRes.data;
      }),
    );
  }

  checkActorInMovie(casts: CastDto[], actorId: number): boolean {
    return casts.reduce(
      (acc, cast) => {
        if (cast.id === actorId) return true;
        return acc;
      },
      false,
    );
  }

  checkMovieInActor(casts: FilmoCastDto[], movieId: number): boolean {
    return casts.reduce(
      (acc, cast) => {
        if (cast.id === movieId) return true;
        return acc;
      },
      false,
    );
  }

  getPopularActor(pages: number): Observable<ActorResultDto> {
    const page = Math.ceil(Math.random() * pages);
    return this.httpService.get('person/popular', {
      params: {
        page,
        language: 'fr-FR',
      },
    }).pipe(
      map((axiosRes: AxiosResponse<ActorDto>) => {
        const actors = axiosRes.data.results.filter((actor) => actor.known_for_department === 'Acting');
        const actor = actors[Math.ceil(Math.random() * (actors.length - 1))];
        return actor;
      }),
    );
  }
}